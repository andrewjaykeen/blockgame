export const ROWS = 20;
export const COLS = 10;
const DEFAULT_STARTING_FALLING_RATE_MS = 250;
const DEFAULT_NUM_CELLS_REQUIRED_TO_MAKE_A_LINE = 4;
const DEFAULT_NUM_STARTING_PIECES = 20;
const MAX_NUM_STARTING_PIECES = 90;

class GameEngine {
  constructor(config = {}) {
    this.fallingRate = DEFAULT_STARTING_FALLING_RATE_MS;
    this.numPiecesToMakeALine = DEFAULT_NUM_CELLS_REQUIRED_TO_MAKE_A_LINE;
    this.amountOfStartingPieces = DEFAULT_NUM_STARTING_PIECES;
    if (config.options) {
      if (config.options.fallingRate)
        this.fallingRate = config.options.fallingRate;
      if (
        config.options.amountOfStartingPieces &&
        config.options.amountOfStartingPieces < MAX_NUM_STARTING_PIECES &&
        config.options.amountOfStartingPieces > 0
      )
        this.amountOfStartingPieces = config.options.amountOfStartingPieces;
      if (config.options.numPiecesToMakeALine)
        this.numPiecesToMakeALine = config.options.numPiecesToMakeALine;
    }

    this.numPiecesRemaining = this.amountOfStartingPieces;
    this.outcome = ''; // won or lost

    this.printDebugInfo = config.printDebugInfo || false;
    this.stateChangedCallback = config.stateChangedCallback || null;

    this.grid = this.generateRandomGrid();
  }

  start = () => {
    this.log('initializing game...');
    this.newFallingPiece();
    this.runLineDetection();
    // this.setState(GAME_STATES.PLAYING);
    this.interval = setInterval(() => {
      this.advancePiece();
    }, this.fallingRate);
  };

  generateRandomGrid = () => {
    let grid = [];

    // empty
    for (let i = 0; i < ROWS; ++i) {
      grid[i] = [];
      for (let j = 0; j < COLS; ++j) {
        grid[i][j] = '';
      }
    }

    let amountRemaining = this.amountOfStartingPieces;
    while (amountRemaining > 0) {
      const randomRow = Math.floor(Math.random() * 7) + 13;
      const randomCol = Math.floor(Math.random() * COLS);
      if (!grid[randomRow][randomCol]) {
        grid[randomRow][randomCol] = this.getRandomColor();
        amountRemaining--;
      }
    }

    return grid;
  };

  quit = outcome => {
    clearInterval(this.interval);
    this.outcome = outcome;
    this.fireStateChangedEvent();
    delete this.stateChangedCallback;
  };

  fireStateChangedEvent = () => {
    if (this.stateChangedCallback) {
      this.stateChangedCallback(this.getGameState());
    }
  };

  getGameState = () => {
    return {
      grid: this.grid,
      fallingPiece: this.fallingPiece,
      numPiecesRemaining: this.numPiecesRemaining,
      outcome: this.outcome,
    };
  };

  rotatePiece = () => {
    if (this.fallingPiece.row1 === this.fallingPiece.row2) {
      this.fallingPiece.row1 = this.fallingPiece.row1 - 1;
      this.fallingPiece.col1 = this.fallingPiece.col2;
    } else if (this.fallingPiece.row1 !== this.fallingPiece.row2) {
      this.fallingPiece.col1 = this.fallingPiece.col1 - 1;
      this.fallingPiece.row1 = this.fallingPiece.row2;
    }
    this.fireStateChangedEvent();
  };

  moveHorizontally = moveRight => {
    const hitRightSide =
      moveRight &&
      (this.fallingPiece.col1 > COLS - 2 || this.fallingPiece.col2 > COLS - 2);
    const hitLeftSide =
      !moveRight && (this.fallingPiece.col1 < 1 || this.fallingPiece.col2 < 1);
    const hitASide = hitLeftSide || hitRightSide;
    if (hitASide) return;

    // if we're moving right, is there a piece to the right? same thing with left
    const targetColumnFirstPiece =
      this.fallingPiece.col1 + (moveRight ? 1 : -1);
    const targetColumnSecondPiece =
      this.fallingPiece.col2 + (moveRight ? 1 : -1);
    const hasPieceToRightOrLeft =
      this.grid[this.fallingPiece.row1][targetColumnFirstPiece] ||
      this.grid[this.fallingPiece.row2][targetColumnSecondPiece];
    if (hasPieceToRightOrLeft) {
      return;
    } else {
      this.fallingPiece.col1 = targetColumnFirstPiece;
      this.fallingPiece.col2 = targetColumnSecondPiece;
      this.fireStateChangedEvent();
    }
  };

  moveDown = () => {
    this.advancePiece(); // only one step
  };

  moveDownAsFarAsYouCan = () => {
    let hitBottom = false;
    while (!hitBottom) {
      hitBottom = this.advancePiece(); // keep on moving down as fast as we can go
    }
  };

  newFallingPiece = () => {
    let color1 = this.getRandomColor();
    let color2 = this.getRandomColor();

    // if there are only a few pieces left, then make it easier by feeding new pieces of the same color.
    // get the color of the first piece on the grid that we find, and just choose that.
    if (this.numPiecesRemaining <= 4) {
      for (let i = 0; i < ROWS; ++i) {
        for (let j = 0; j < COLS; ++j) {
          if (this.grid[i][j]) {
            color1 = this.grid[i][j];
            color2 = this.grid[i][j];
          }
        }
      }
    }
    this.fallingPiece = {
      row1: 0,
      col1: 5,
      color2,
      color1,
      row2: 0,
      col2: 6,
    };
  };

  getRandomColor = () => {
    const randomNumber = Math.floor(Math.random() * 4) + 1;
    if (randomNumber === 1) return 'R';
    if (randomNumber === 2) return 'G';
    if (randomNumber === 3) return 'Y';
    if (randomNumber === 4) return 'B';
  };

  clearPiece = (row, col) => {
    this.grid[row][col] = '';
    --this.numPiecesRemaining;
    this.fireStateChangedEvent();
  };

  placePiecePermanently = (row, col, color) => {
    this.grid[row][col] = color;
    ++this.numPiecesRemaining;
    this.fireStateChangedEvent();
  };

  runLineDetection = () => {
    let lineDetected = false;

    // identify any lines that can be cleared from the grid.  then make them disappear.
    for (let i = 0; i < ROWS; ++i) {
      for (let j = 0; j < COLS; ++j) {
        const color = this.grid[i][j];
        if (color) {
          // there's something in the cell, so now check around it (first right, then down)

          // check to the right, be greedy
          let stoppedMatching = false;
          let numContiguous = 1;
          for (let jj = 1; jj < COLS; ++jj) {
            if (
              this.grid[i] &&
              this.grid[i][j + jj] &&
              this.grid[i][j + jj] === color
            ) {
              // matches
              if (!stoppedMatching) {
                ++numContiguous;
              }
            } else {
              stoppedMatching = true;
            }
          }

          let madeLineToRight = numContiguous >= this.numPiecesToMakeALine;

          if (madeLineToRight) {
            lineDetected = true;
            // console.log('FOUND A HORIZONTAL LINE!');
            // clear them out
            for (let jj = 0; jj < numContiguous; ++jj) {
              this.clearPiece(i, j + jj);
            }
          }

          // check for vertical lines
          let stoppedMatchingDown = false;
          let numContiguousDown = 1;

          for (let ii = 1; ii < ROWS; ++ii) {
            if (
              this.grid[i + ii] &&
              this.grid[i + ii][j] &&
              this.grid[i + ii][j] === color
            ) {
              // matches
              if (!stoppedMatchingDown) {
                ++numContiguousDown;
              }
            } else {
              stoppedMatchingDown = true;
            }
          }

          let madeLineDownward = numContiguousDown >= this.numPiecesToMakeALine;

          if (madeLineDownward) {
            lineDetected = true;
            // console.log('FOUND A VERTICAL LINE!');
            // clear them out
            for (let ii = 0; ii < numContiguousDown; ++ii) {
              this.clearPiece(i + ii, j);
            }
          }
        }
      }
    }

    if (lineDetected) {
      // // var audio = new Audio('audio/deathray.mp3');
      // try {
      //   var audio = new Audio('audio/raygun.mp3');
      //   audio.volume = 0.2;
      //   audio.play();
      // } catch (e) {
      //   // stupid iphone
      // }
    }
  };

  advancePiece = () => {
    let returnValue = false;

    // is there one directly below this piece already (or did it hit the bottom)?
    const itHitBottom = (this.fallingPiece.row1 > ROWS - 2) || (this.fallingPiece.row2 > ROWS - 2);
    if (
      itHitBottom ||
      (this.grid[this.fallingPiece.row1 + 1][this.fallingPiece.col1]) ||
      (this.grid[this.fallingPiece.row2 + 1][this.fallingPiece.col2])
    ) {
      // 1) stop it, place it permanently into this.grid at the right location
      this.placePiecePermanently(
        this.fallingPiece.row1,
        this.fallingPiece.col1,
        this.fallingPiece.color1
      );
      this.placePiecePermanently(
        this.fallingPiece.row2,
        this.fallingPiece.col2,
        this.fallingPiece.color2
      );

      this.runLineDetection();

      // detect if the user has won.
      if (this.numPiecesRemaining < 1) {
        this.quit('won');
      }

      // 2) drop a new random piece from the top
      if (!this.grid[0][5]) {
        this.newFallingPiece();
      } else {
        // player lost
        this.quit('lost');
      }
      returnValue = true; // the piece either hit bottom or there's something underneath it
    } else {
      // advance it downward
      this.fallingPiece.row1 = this.fallingPiece.row1 + 1;
      this.fallingPiece.row2 = this.fallingPiece.row2 + 1;
    }

    this.fireStateChangedEvent();
    return returnValue;
  };

  log = msg => {
    if (this.printDebugInfo) console.log('Engine: ' + msg);
  };
}

export default GameEngine;
