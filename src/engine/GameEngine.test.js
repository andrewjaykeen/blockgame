import GameEngine, { ROWS, COLS } from './GameEngine';
import sinon from 'sinon';

function noop() {
  return {};
}

describe('constructor', () => {
  it('new game honors options', () => {
    const game = new GameEngine({});
    game.start();
    expect(game.grid.length).toEqual(20);
  });
});

describe('game play tests', function() {
  let clock;

  beforeEach(function() {
    clock = sinon.useFakeTimers();
  });

  afterEach(function() {
    clock.restore();
  });

  it('starts moving pieces', () => {
    const game = new GameEngine({});
    game.start();
    let lastFallingPiece;
    for (let i = 1; i < 20; ++i) {
      const gameState = game.getGameState();
      const fallingPiece = Object.assign({}, gameState.fallingPiece);
      clock.tick(250);
      if (lastFallingPiece) {
        expect(fallingPiece.row1).not.toEqual(lastFallingPiece.row1);
        // console.log(`${i}: ${fallingPiece.row}`);
      }
      lastFallingPiece = fallingPiece;
    }
  });

  it('should lose the game after a while if you do not move', () => {
    const game = new GameEngine({});
    game.start();
    clock.tick(500 * 1000); // 500 seconds
    const gameState = game.getGameState();
    expect(gameState.outcome).toEqual('lost');
  });

  // it('should be able to win a game', () => {
  //   const game = new GameEngine({});
  //
  //   // create an empty grid
  //   let grid = [];
  //   for (let i = 0; i < ROWS; ++i) {
  //     grid[i] = [];
  //     for (let j = 0; j < COLS; ++j) {
  //       grid[i][j] = '';
  //     }
  //   }
  //
  //   // add a single piece to the last row, right where the next piece will fall, and update the internal game state
  //   grid[ROWS - 1][5] = 'G';
  //   game.grid = grid;
  //   game.numPiecesRemaining = 1;
  //
  //   // NOTE: this only really works because there's logic in newFallingPiece() that makes it way easier to finish
  //   // a game when you get down to the last few pieces.  solving a random game programatically would be much more effort.
  //
  //   game.start();
  //   clock.tick(500 * 1000); // 500 seconds
  //   const gameState = game.getGameState();
  //   expect(gameState.outcome).toEqual('won');
  // });
});
