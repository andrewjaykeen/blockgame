import React, { Component } from 'react';
import { isMobile } from 'react-device-detect';

import './App.css';
import Grid from './components/Grid';
import Controls from './components/Controls';
import GameEngine from './engine/GameEngine';

class App extends Component {
  state = {
    game: null,
    gameState: null,
    loading: true,
    options: {
      fallingRate: 200,
      numPiecesToMakeALine: 4,
      amountOfStartingPieces: 40,
    },
  };

  componentDidMount() {
    const self = this;
    const newGame = new GameEngine({
      stateChangedCallback: s => {
        self.setState({
          gameState: s,
        });
      },
      options: this.state.options,
    });
    newGame.start();
    self.setState({
      game: newGame,
      gameState: newGame.getGameState(),
      loading: false,
    });

    document.addEventListener(
      'keydown',
      e => {
        if (
          e.keyCode === 37 ||
          e.keyCode === 38 ||
          e.keyCode === 39 ||
          e.keyCode === 40
        ) {
          e.preventDefault(); // prevent scrolling
          if (e.keyCode === 37 || e.keyCode === 39) {
            self.state.game.moveHorizontally(e.keyCode !== 37);
          } else if (e.keyCode === 40) {
            self.state.game.moveDownAsFarAsYouCan();
          } else if (e.keyCode === 38) {
            self.state.game.rotatePiece();
          }
        }
      },
      false
    );
  }

  render() {
    const self = this;
    const gameState = this.state.gameState;

    if (this.state.loading) {
      return <div>loading</div>;
    }
    const { outcome } = gameState;

    return (
      <div className="App">
        {outcome && <div className="outcome">You have {outcome} the game.</div>}
        <Grid data={gameState.grid} fallingPiece={gameState.fallingPiece} />

        {!isMobile && (
          <button
            onClick={() => {
              self.state.game.quit();
              const newGame = new GameEngine({
                stateChangedCallback: s => {
                  self.setState({
                    gameState: s,
                  });
                },
                options: this.state.options,
              });
              newGame.start();
              self.setState({
                game: newGame,
                gameState: newGame.getGameState(),
              });
            }}
            style={{ margin: '20px auto', display: 'block' }}
          >
            New Game
          </button>
        )}

        {!isMobile && (
          <div className="piecesRemaining">
            Pieces: {this.state.gameState.numPiecesRemaining}
          </div>
        )}

        {isMobile && (
          <Controls
            onLeftClick={() => {
              this.state.game.moveHorizontally(false);
            }}
            onRightClick={() => {
              this.state.game.moveHorizontally(true);
            }}
            onDownClick={() => {
              this.state.game.moveDownAsFarAsYouCan();
            }}
            onRotateClick={() => {
              this.state.game.rotatePiece();
            }}
          />
        )}
      </div>
    );
  }
}

export default App;
