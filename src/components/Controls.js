import React from 'react';
import PropTypes from 'prop-types';

function Controls({ onLeftClick, onRightClick, onRotateClick, onDownClick }) {
  return (
    <div className="controls">
      <div className="leftRightArrows">
        <div onClick={onLeftClick}>⇦</div>
        <div onClick={onRotateClick}>⟲</div>
        <div onClick={onRightClick}>⇨</div>
      </div>
      <div className="downArrow" onClick={onDownClick}>
        ⇩
      </div>
    </div>
  );
}

Controls.propTypes = {
  onLeftClick: PropTypes.func.isRequired,
  onRightClick: PropTypes.func.isRequired,
  onDownClick: PropTypes.func.isRequired,
  onRotateClick: PropTypes.func.isRequired,
};

export default Controls;
