import React from 'react';
import PropTypes from 'prop-types';

function Grid({ data, fallingPiece }) {
  if (data) {
    return (
      <div className="gameGrid">
        {data.map((row, i) => {
          return (
            <div className="row" key={i}>
              {row.map((col, j) => {
                let color = col;
                if (
                  fallingPiece &&
                  fallingPiece.row1 === i &&
                  fallingPiece.col1 === j
                ) {
                  color = fallingPiece.color1;
                }
                if (
                  fallingPiece &&
                  fallingPiece.row2 === i &&
                  fallingPiece.col2 === j
                ) {
                  color = fallingPiece.color2;
                }
                return <div className={`col ${color}`} key={j} />;
              })}
            </div>
          );
        })}
      </div>
    );
  }
  return null;
}

Grid.propTypes = {
  data: PropTypes.array,
  fallingPiece: PropTypes.shape({
    row1: PropTypes.number,
    col1: PropTypes.number,
    color1: PropTypes.oneOf(['R', 'Y', 'G', 'B']),
    color2: PropTypes.oneOf(['R', 'Y', 'G', 'B']),
    row2: PropTypes.number,
    col2: PropTypes.number,
  }),
};

export default Grid;
