DONE - moveHorizontally - need collision detection with side or existing pieces

DONE - line detection - if the user gets X in a row horizontally or vertically, then clear them out
    as a first step we should just clear out the ones you got
DONE: run line detection on first load to clear out any starting lines
DONE - make it work on mobile vertically
DONE - moveDown - if the user presses the down arrow it goes straight down as far as it can go and locks into place
DONE - line detection - be more greedy (keep searching for more cells that make up a line!)
DONE: set up bitbucket CI
DONE: heroku deployment
DONE: focus on user experience / gameplay
   DONE - allow you to set the difficulty level (number of starting pieces)
   DONE - allow you to set the speed (falling piece rate)
   DONE - allow user to control amount of blocks to make a line (this should be part of level advancement later on..
   DONE display count of remaining pieces to the user (count all the non empty cells in the grid)
   DONE: when you get down to the last 1 or 2 remaining blocks, then the falling pieces should be those colors so you can
                        finish it out quickly
    DONE: an overlay when they win (switch game state to ended, game result: failed)
DONE: unit test the game engine as-is so far
    use sinon fakeTimers for setTimeout testing

DONE: make a README file
DONE: add blue color to the game and change the color scheme.

TODO: try making the falling pieces be block clusters (dr mario clone)
    DONE: add mobile controls to rotate piece
    DONE: squash the commits, merge the branch and deploy to heroku
    TODO: rotatePiece() needs to not just flip between two states..
    TODO: gravity (pieces should fall if they were originally falling pieces - should we add a flag when placing the pieces permanently?)
    TODO: fix unit tests

TODO: focus on user experience / gameplay
   TODO concept of levels with gradual difficulty (timer and score accumulation)
    TODO score feature - show it on the right
    TODO: timer/counter of some kind
    TODO: integrate the pieces remaining feature
    sound effects (problems on mobile...)

TODO: animate the falling piece and put this.animating = true or something
TODO - line detection - pretend there's "gravity" and make the pieces fall.  will require looping the lineDetection() function
    until no more pieces have been adjusted.
        NOTE: this will likely be much better UX if there's an animation.
           therefore a new game state like ANIMATING where you're not allowed to move, etc, will allow us to avoid letting the user move
TODO - ability to pause game play
    stop the setInterval and restart it on unpause
    prevent all the move() commands from working if the game state is PAUSED

IDEA - in addition to color, put a shape or symbol on each block so you have to line up 3 of any symbols or 4 of any color
    or, something to that effect.  some combination thereof.test
test
