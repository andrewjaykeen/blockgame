## Andy's Block Game

A single-player falling block game, in the style of Tetris or Dr. Mario.

Live Demo: https://andysblocks.herokuapp.com

This project uses create-react-app for the UI.  The game engine is pure ES6.

## Features

* Works on mobile web browsers (delivering different UX on mobile and desktop)
* Uses only 662 lines of JS+CSS for *everything* (game engine, UI components and unit tests).
* Game Engine is completely decoupled from UI and entirely event-driven (no polling)
* Unit tests for the game engine test actual end-to-end game play (winning/losing games, using sinon fakeTimers)
* Uses prettier, prop-types, lodash, jest, enzyme and sinon.
* Bitbucket CI pipeline
* Deployed via Heroku

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

For code coverage tests: `yarn test --coverage --watchAll=false`

Note: There's a bug in jest, so you must turn *off* watching when running CC
 
https://github.com/facebook/create-react-app/issues/6888


### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
